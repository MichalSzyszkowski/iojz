## Copyright (C) 2017 michal
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} UtworzWektoryObserwacji (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: michal <michal@michal-U24E>
## Created: 2017-06-04

function [listaTrenujaca] = UtworzWektoryObserwacji (Pliki)

listaTrenujaca = [];
liczbaTwarzy = rows(Pliki);
for i = 1:liczbaTwarzy
    %deblank konieczne - octave tworzy stringi o tej samej dlugosci i doklade puste znaki na koncu
    macierz = imread(deblank(Pliki(i,1:end)));
    wektor = reshape(macierz, 1, []);
    listaTrenujaca = [listaTrenujaca; wektor]; # USUN: ograniczenie wektora, aby przyspieszyc obliczenia
endfor

endfunction
