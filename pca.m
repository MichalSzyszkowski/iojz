function [WektoryWlasne, WartosciWlasne] = pca(A, J)

liczbaWierszy = size(A,1);

% Odjac od macierzy srednia z wierszy
% (Stworzenie macierzy odchylen)
for i = 1:liczbaWierszy
	A(i, :) = A(i, :) - mean(A(i,:));
end

% Wyliczenie macierzy kowariancji
X = cov(A);

%Wyliczenie wartosci wlasnych
[WektoryWlasne WartosciWlasne] = eigs(X, J);

endfunction


