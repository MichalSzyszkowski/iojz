﻿Wektory twarz (eigenfaces):
	https://www.youtube.com/watch?v=_lY74pXWlS8
Wyjasnienie k-means clustering (k-średnich):
	https://www.youtube.com/watch?v=_aWzGGNrcic
Matlab k-means:
	https://www.mathworks.com/help/stats/kmeans.html?requestedDomain=www.mathworks.com
Plik AccMeasure:
	http://www.mathworks.com/matlabcentral/fileexchange/32197-clustering-results-measurement?focused=6559077&tab=function
Wyjasnienie k-NN:
	https://www.youtube.com/watch?v=4ObVzTuFivY
Matlab klasyfikator k-NN:
	https://www.mathworks.com/help/stats/classificationknn-class.html
Matlab predict k-NN:
	https://www.mathworks.com/help/stats/classificationknn.predict.html


Zad 2
orl database - 400 obrazow twarzy
zaczac z 3 z 40 grup, sprobowac rozszerzyc do 10 grup
wszystkie twarze wrzucic do jednej macierzy i uzyc PCA
uzyc funkcje kmeans (po polsku metoda k-srednich) - uzyskane wyniki uzyj dla funkcji AccMeasure(,) i evalclusters()
grupa treningowa(1/5) i grupa testujaca(4/5)

J - liczba eigenvectorów eigs(M,J)
im J jest wyzsze tym obraz po odtworzeniu bedzie blizszy oryginalowi
