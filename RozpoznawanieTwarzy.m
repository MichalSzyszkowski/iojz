## Copyright (C) 2017 michal
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} RozpoznawanieTwarzy (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: michal <michal@michal-U24E>
## Created: 2017-06-05

function [retval] = RozpoznawanieTwarzy (J, LiczbaIteracji, LiczbaZdjec)

%Wczytanie plikow
Podkatalogi = ['s1/';'s2/';'s3/';'s5/';'s6/';'s8/';'s7/';'s9/';'s10/';'s11/'];
Podkatalogi = Podkatalogi(1:LiczbaZdjec,:);
KatalogTwarzyTrenujacych = 'twarzeTrenujace/';
KatalogTwarzyTestujacych = 'twarzeTestujace/';

PoprawnePlikiTrenujace = OtworzPliki(KatalogTwarzyTrenujacych, Podkatalogi);
PoprawnePlikiTestujace = OtworzPliki(KatalogTwarzyTestujacych, Podkatalogi);

%Utworzenia z macierzy obrazow wektorow obserwacji
listaTrenujaca = UtworzWektoryObserwacji(PoprawnePlikiTrenujace);
listaTestujaca = UtworzWektoryObserwacji(PoprawnePlikiTestujace);

%Stworzenie twarzy wlasnych - metoda PCA
twarzeWlasne = UtworzTwarzeWlasne(listaTrenujaca, J);
twarzeWlasneTestujace = UtworzTwarzeWlasne(listaTestujaca, J);

liczbaGrupTwarzy = rows(Podkatalogi);

%Wyliczenie grup w jakich powinny byc obrazy
PrawdziweRezultaty = PrzydzielDoPrawdziwychGrup (PoprawnePlikiTrenujace, 
                        listaTrenujaca, KatalogTwarzyTrenujacych);
                        
                       
%Bez PCA
skutecznoscBezPCA = 0;
czas = time();
for i = 1:LiczbaIteracji
BezPCARezultaty = kmeans(listaTrenujaca, liczbaGrupTwarzy);
[skutecznoscBezPCAtemp, rand_index, match] = AccMeasure(PrawdziweRezultaty, BezPCARezultaty);
skutecznoscBezPCA = skutecznoscBezPCA + (skutecznoscBezPCAtemp/LiczbaIteracji);
endfor
CzasDzialaniaBezPCA = (time() - czas)/LiczbaIteracji
skutecznoscBezPCA

%PCA
skutecznoscPCA = 0;
czas = time();
for i = 1:LiczbaIteracji
PCARezultaty = kmeans(twarzeWlasne, liczbaGrupTwarzy);
[skutecznoscPCAtemp, rand_index, match] = AccMeasure(PrawdziweRezultaty, PCARezultaty);
skutecznoscPCA = skutecznoscPCA + (skutecznoscPCAtemp/LiczbaIteracji);
endfor
CzasDzialaniaPCA = (time() - czas)/LiczbaIteracji
skutecznoscPCA

%Dodanie informacji o zaklasyfikowaniu zbiory trenujacego do grup. Potrzebne dla KNN
PozycjaGrupy = columns(listaTrenujaca) + 1;
listaTrenujaca(:, PozycjaGrupy) = PrawdziweRezultaty;

%KNN
PoprawneZaklasyfikowania = 0;
czas = time();
for j = 1:LiczbaIteracji
  for i=1:rows(listaTestujaca)
    [zaklasyfikowanie, k, dist, idx] = fastKNN(listaTrenujaca, listaTestujaca(i,:), 1);
    Grupa = ZaklasyfikujPlikDoGrupy(PoprawnePlikiTestujace(i,:), KatalogTwarzyTestujacych); 
    if(zaklasyfikowanie==Grupa)
      PoprawneZaklasyfikowania = PoprawneZaklasyfikowania + 1;
    endif
  endfor
endfor

CzasDzialaniaKNN = (time() - czas)/LiczbaIteracji
SkutecznoscKNN = (100*PoprawneZaklasyfikowania / rows(listaTestujaca))/LiczbaIteracji

PozycjaGrupyPCA = columns(twarzeWlasneTestujace) + 1;
twarzeWlasne(:, PozycjaGrupyPCA) = PrawdziweRezultaty;
PoprawneZaklasyfikowaniaPCA = 0;
czas = time();
for j = 1:LiczbaIteracji
  for i=1:rows(twarzeWlasneTestujace)
    [zaklasyfikowanie, k, dist, idx] = fastKNN(twarzeWlasne, twarzeWlasneTestujace(i,:), 1);
    Grupa = ZaklasyfikujPlikDoGrupy(PoprawnePlikiTestujace(i,:), KatalogTwarzyTestujacych); 
    if(zaklasyfikowanie==Grupa)
      PoprawneZaklasyfikowaniaPCA = PoprawneZaklasyfikowaniaPCA + 1;
    endif
  endfor
endfor

CzasDzialaniaKNNPCA = (time() - czas)/LiczbaIteracji
SkutecznoscKNNPCA = (100*PoprawneZaklasyfikowaniaPCA / rows(twarzeWlasneTestujace))/LiczbaIteracji

retval = 0;
endfunction
