#TODO
# Stworzyc kilka grup twarzy (mozna 3, potem 5, 7, 10)
# Dla kazdej grupy sprawdzic efektywnosc metoda kmeans, k-nn
# Dla Kmeans , J = 4,10,20,30
# Dla kmeansa wrzucic albo twarz albo wektor twarzy
# Porównać dokładność i czas grupowania
# Porównać k-means i k-nn

# Eigenface
# WektoryWlasne, WartosciWlasne
#
# stworzyc listaTrenujaca(1/5 grupy) i uzyc ja do stworzenia WektoryWlasne
# stworzyc listaTestujaca(4/5 grupy) i uzyc ja do stworzenia WektoryTwarzy

# k-nn tworzymy dajac obserwacje i klastry
# trueResults -stworzyc recznie
# trueResults - stworzyc klaster zawierajacy poprawne grupowanie
#[acc, rand_index, match] = AccMeasure(trueResults, results)

#im wieksze J tym AccMeasure powinien zwrocic wiekszy procent poprawnych wynikow

pkg load statistics

clear;
clc;

LiczbaIteracji = 10;
GrupyZdjec = [3,5,7]; %Maksimum 10
WartosciJ = [4,10,20,30];

for i = 1:length(WartosciJ)
  for  g = 1:length(GrupyZdjec)
    J = WartosciJ(i)
    RozpoznawanieTwarzy(J, LiczbaIteracji, GrupyZdjec(g));
  endfor
endfor
